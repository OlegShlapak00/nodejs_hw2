const express = require('express');
const router = express.Router();

const authMiddleware = require('../middleware/authMiddleware');

const {getNotes, addNote,getNoteById, updateNote, checkNote, deleteNote} = require("../controllers/notesControler");

router.get('/notes', authMiddleware, getNotes);
router.get('/notes/:id', authMiddleware, getNoteById);
router.post('/notes', authMiddleware, addNote);

 router.put('/notes/:id', authMiddleware, updateNote);
 router.patch('/notes/:id', authMiddleware, checkNote);
 router.delete('/notes/:id', authMiddleware, deleteNote);


module.exports = router;
