const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('note', new Schema({
    userId: {
        required: true,
        type: String,
    },
    completed: {
        type: Boolean
    },
    createdDate: {
        type: Date
    },
    text: {
        required: true,
        type: String
    }
}));
