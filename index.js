const express = require("express");
const mongoose = require("mongoose");
const app = express();

const dbConfig = require("./configs/db");
const userRouter = require("./routers/userRouter");
const authRouter = require("./routers/authRouter");
const notesRouter = require("./routers/notesRouter");


mongoose.connect(`mongodb+srv://user:12345@cluster0.hnyca.mongodb.net/NODEJS_HW2?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(express.json());

app.use("/api",notesRouter);
app.use("/api",userRouter);
app.use("/api",authRouter);


app.listen(8080||process.env.PORT , () => console.log("listening"));
