const User = require('../models/user');
const jwt = require("jsonwebtoken");
const Credential = require('../models/credentials');


module.exports.getMe = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userName = jwt.decode(token).username;
    User.findOne({"username": userName},{__v:0},  (err, user) => {
            if (!user) {
                return response.status(400).json({massage: "string"})
            }
            return response.status(200).json({user: user})
        }
    ).catch(() => {
        return response.status(500).json({massage: "string"})
    })
}

module.exports.deleteMe = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userName = jwt.decode(token).username;
    Credential.deleteOne({"username": userName})
        .then(() => {
            User.deleteOne({"username": userName})
                .then(() => {
                        return response.status(200).json({massage: "Success"});
                    }
                )
                .catch(() => {
                        return response.status(500).json({massage: "string"});
                    }
                )
        })
        .catch(() => {
                return response.status(500).json({massage: "string"});
            }
        );
}

module.exports.changePassword = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userName = jwt.decode(token).username;
    const {oldPassword, newPassword} = request.body;
    Credential.updateOne({"username": userName}, {password: newPassword})
        .then(() => {
            if (!oldPassword || !newPassword) {
                return response.status(400).json({massage: "string"});
            }
            return response.status(200).json({massage: "Success"});
        })
        .catch(() => {
                return response.status(500).json({massage: "string"});
            }
        );
}
