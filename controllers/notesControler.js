const Notes = require("../models/note");
const jwt = require("jsonwebtoken");


module.exports.getNotes = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    Notes.find({userId: userId}, {__v: 0})
        .exec()
        .then(notes => {
            if (!notes) {
                return response.status(400).json({massage: "string"});
            }
            return response.status(200).json({notes: notes});
        })
        .catch(() => {
            response.status(500).json({massage: "string"});
        });
}
module.exports.getNoteById = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    const noteId = request.params.id;
    Notes.findOne({userId: userId, _id: noteId}, {__v: 0})
        .exec()
        .then(note => {
            if (!note) {
                return response.status(400).json({massage: "string"});
            }
            return response.status(200).json({note: note});
        })
        .catch(() => {
            response.status(500).json({massage: "string"});
        });
}

module.exports.addNote = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    const {text} = request.body;
    const createdDate = new Date();
    const completed = false;
    const note = new Notes({userId, completed, createdDate, text});
    note.save()
        .then(() => {
                if (!text) {
                    return response.status(400).json({massage: "string"})
                }
                return response.status(200).json({massage: "Success"})
            }
        )
        .catch(() => {
            response.status(500).json({massage: "string"});
        });
}

module.exports.updateNote = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    const noteId = request.params.id;
    const {text} = request.body;
    if(!text){
        return response.status(400).json({massage: "string"});
    }
    Notes.updateOne({userId: userId, _id: noteId}, {text: text})
        .exec()
        .then(note => {
            if (!note) {
                return response.status(400).json({massage: "string"});
            }
            return response.status(200).json({massage: "Success"});
        })
        .catch(() => {
            return response.status(500).json({massage: "string"});
        });
}

module.exports.checkNote = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    const noteId = request.params.id;
    Notes.findOne({userId: userId, _id: noteId})
        .then((note) => {
            if(!note){
                return response.status(400).json({massage: "string"});
            }
            note.completed = !note.completed;
            note.save();
            return response.status(200).json({massage: "Success"});
        })
        .catch(() => {
            return response.status(500).json({massage: "string"});
        });
}

module.exports.deleteNote = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id;
    const noteId = request.params.id;
    Notes.deleteOne({userId: userId, _id: noteId})
        .then((note) => {
            if(!note){
                return response.status(400).json({massage: "string"});
            }
            return response.status(200).json({massage: "Success"});
        })
        .catch(() => {
            return response.status(500).json({massage: "string"});
        });
}
