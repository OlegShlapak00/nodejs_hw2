const jwt = require('jsonwebtoken');
const User = require('../models/user');
const Credential = require('../models/credentials');

const {secret} = require('../configs/auth');


module.exports.register = (request, response) => {
    const {username, password} = request.body;
    const createdDate = new Date();
    const user = new User({username, createdDate});
    const credential = new Credential({username, password});
    credential.save()
        .then(() => {
            if (!username || !password) {
              return   response.status(400).json({massage: "string"});
            }
            user.save()
                .catch(() => {
                   return  response.status(500).json({massage: "string"});
                });
            return  response.json({massage: 'Success'});
        })
        .catch(() => {
           return  response.status(500).json({massage: "string"});
        });
}

module.exports.login = (request, response) => {

    const {username, password} = request.body;

    Credential.findOne({username, password}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({massage: "string"});
            }
            return  response.json({massage: 'success', token: jwt.sign(JSON.stringify(user), secret)});
        })
        .catch(() => {
            return  response.status(500).json({massage: "string"});
        });
}
